import audio from '@ohos.multimedia.audio'
import media from '@ohos.multimedia.media'
import { CommonChangedListener, MediaRecordState } from '../../../common/constants/MediaConstants'

export default interface IMediaRecord {
  loadConfig(config: media.AVRecorderConfig | audio.AudioCapturerOptions)

  start(saveFileDesc?: number)

  pause()

  resume()

  stop()

  reset()

  release()

  addStatusChangedListener(callback: CommonChangedListener<MediaRecordState>): void

  getRecorderState(): MediaRecordState

  isRecording(): boolean
}