import audio from '@ohos.multimedia.audio'
import { afterEach } from '@ohos/hypium';
import { MediaRecordState } from '../../../common/constants/MediaConstants';
import fs from '@ohos.file.fs';
import { IMediaRecordProxy } from './IMediaRecordProxy';

var audioCollectorOptions = {
  streamInfo: {
    samplingRate: audio.AudioSamplingRate.SAMPLE_RATE_44100,
    channels: audio.AudioChannel.CHANNEL_2,
    sampleFormat: audio.AudioSampleFormat.SAMPLE_FORMAT_S16LE,
    encodingType: audio.AudioEncodingType.ENCODING_TYPE_RAW
  },
  capturerInfo: {
    source: audio.SourceType.SOURCE_TYPE_MIC,
    capturerFlags: 0
  }
}

export default class AudioCollector extends IMediaRecordProxy {
  private audioCollector: audio.AudioCapturer;
  private saveFileDesc: number;
  private isBlockingRead: boolean = true;
  private collectOffsetIndex: number = 0;

  public constructor() {
    super()
    this.initCapturer(audioCollectorOptions);
  }

  private setTaskCallback() {
    this.audioCollector.on('markReach', 1000, (reachNumber) => {
      console.info('cwq Mark reach event Received');
      console.info(`cwq The Capturer reached frame: ${reachNumber}`);
    });
    this.audioCollector.on('periodReach', 1000, (reachNumber) => {
      console.info('cwq Period reach event Received');
      console.info(`cwq In this period,the Capturer reached frame: ${reachNumber}`);
    });
    this.audioCollector.on('stateChange', async (state) => {
      switch (state) {
        case audio.AudioState.STATE_INVALID:
          break;
        case audio.AudioState.STATE_NEW:
          this.stepCallback(MediaRecordState.IDLE)
          break;
        case audio.AudioState.STATE_PREPARED:
          this.isReady = true;
          this.stepCallback(MediaRecordState.PREPARED);
          break;
        case audio.AudioState.STATE_RUNNING:
          this.startDurationTasker();
          this.stepCallback(MediaRecordState.START);
          this.collectBuffer();
          break;
        case audio.AudioState.STATE_STOPPED:
          this.stepCallback(MediaRecordState.STOP, this.curRecordAsset);
          this.stopDurationTasker();
          fs.closeSync(this.saveFileDesc);
          this.saveFileDesc = null;
          this.collectOffsetIndex = 0;
          if (this.isUseLibraryUrl) {
            this.isUseLibraryUrl = false;
            this.mediaLibOperator.closeOMediaFileOperation(this.curRecordAsset.getAsset(), this.curRecordAsset.getFd());
this.curRecordAsset = null;
          }
        break;
        case audio.AudioState.STATE_RELEASED:
        this.isReady = false;
        this.stepCallback(MediaRecordState.RELEASE);
        break;
      }
    });
  }
  private saveStream(stream:ArrayBuffer){
    fs.writeSync(this.saveFileDesc,stream,{
      offset:this.collectOffsetIndex*stream.byteLength,
      length:stream.byteLength
    });
    this.collectOffsetIndex++
  }
  private async initCapturer(options:audio.AudioCapturerOptions){
    this.audioCollector = await audio.createAudioCapturer(options);
    this.setTaskCallback();
  }
  async loadConfig(options:audio.AudioCapturerOptions){
    this.reset();
    audioCollectorOptions = options;
    await this.initCapturer(audioCollectorOptions);
  }
  private async collectBuffer(){
    while (this.getRecorderState() ===MediaRecordState.START){
      let bufferSize = await this.audioCollector.getBufferSize();
      let buffer:ArrayBuffer = await this.audioCollector.read(bufferSize,this.isBlockingRead);
      this.saveStream(buffer);
      this.stepCallback(MediaRecordState.COLLECT_BUFFER,buffer);
    }
  }
  public async start(saveFileDesc?:number){
    await this.stop()
    this.saveFileDesc = saveFileDesc??this.saveFileDesc??(await this.getLibraryFileDesc(true));
    if(this.isReady){
      await this.audioCollector.start();
    }
  }
  public pause(){
    this.stepCallback(MediaRecordState.PAUSE);
  }
  public resume(){
    this.stepCallback(MediaRecordState.START);
  }
  public async stop(){
    if(this.getRecorderState()===MediaRecordState.START){
      await this.audioCollector.stop();
    }
  }
  reset(){
    this.stop();
  }
  public async release(){
    this.stop();
    if(this.isReady){
      await this.audioCollector.release();
      this.audioCollector.off('markReach');
      this.audioCollector.off('periodReach');
    }
    super.release();
  }
}