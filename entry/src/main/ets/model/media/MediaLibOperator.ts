import mediaLibrary from '@ohos.multimedia.mediaLibrary'
import common from '@ohos.app.ability.common'
import { MediaType } from '../../common/constants/MediaConstants';
import { MediaOperationAsset } from '../../common/been/MediaOperationAsset';

export default class MediaLibOperator{
  private sysMediaLib:mediaLibrary.MediaLibrary
  public constructor(context :common.Context) {
this.sysMediaLib = mediaLibrary.getMediaLibrary(context);
  }
  private getSaveDir(mediaType:MediaType){
    switch (mediaType){
      case MediaType.IMAGE:
      return mediaLibrary.DirectoryType.DIR_IMAGE
      case MediaType.AUDIO:
        return mediaLibrary.DirectoryType.DIR_AUDIO
      case MediaType.VIDEO:
        return mediaLibrary.DirectoryType.DIR_VIDEO
      case MediaType.FILE:
      return mediaLibrary.DirectoryType.DIR_DOCUMENTS
    }
  }

  async createMediaFile(creationName,mediaType:MediaType):Promise<MediaOperationAsset>{
let publicPath = await this.sysMediaLib.getPublicDirectory(this.getSaveDir(mediaType));
    let mediaBuilderAsset = await this.sysMediaLib.createAsset(mediaType.valueOf(),creationName,publicPath);
    let fd = await this.openMediaFileOperation(mediaBuilderAsset,'rw')
    if(fd>0){
      let operationAsset = new MediaOperationAsset()
      operationAsset.setAsset(mediaBuilderAsset)
      operationAsset.setFd(fd)
      return operationAsset
    }
  }

  async openMediaFileOperation(mediaFileAsset:mediaLibrary.FileAsset,operation):Promise<number>{
    if(mediaFileAsset!=null){
      let operationAuth = await mediaFileAsset.open(operation);
      if(operationAuth>0){
        return operationAuth
      }
    }
  }

  async closeOMediaFileOperation(mediaFileAsset:mediaLibrary.FileAsset,fd):Promise<void>{
    if(mediaFileAsset !=null){
      mediaFileAsset.close(fd)
    }
  }

  async openMediaFileOperationById(fileId):Promise<MediaOperationAsset>{
    let fileKeyObj = mediaLibrary.FileKey;
    let fetchOp = {
      selections :fileKeyObj.ID+'= ?',
      selectionArgs:[fileId.toString()],
      order:fileKeyObj.DATE_ADDED +" DESC",
    }
    let fetchFileResult = await this.sysMediaLib.getFileAssets(fetchOp);
    let fileAsset = await fetchFileResult.getFirstObject();
    let fd = await this.openMediaFileOperation(fileAsset,'r')
    if(fd >0){
      let operationAsset = new MediaOperationAsset()
      operationAsset.setAsset(fileAsset)
      operationAsset.setFd(fd)
      return operationAsset
    }
  }
  async getAsset(mediaType,fileId){
    let fileKeyObj = mediaLibrary.FileKey;
    let fetchOp = {
      selections:fileKeyObj.ID+'= ?',
      selectionArgs:[fileId.toString()],
    };
    let fetchFileResult = await this.sysMediaLib.getFileAssets(fetchOp);
    let fileAsset = await fetchFileResult.getFirstObject();
    return fileAsset
  }
 async getAllAssets(mediaTypes?:MediaType[]):Promise<Array<mediaLibrary.FileAsset>>{
   if(mediaTypes == null){
     mediaTypes = []
   }
   if(mediaTypes.length ==0){
     mediaTypes.push(MediaType.FILE)
     mediaTypes.push(MediaType.IMAGE)
     mediaTypes.push(MediaType.AUDIO)
     mediaTypes.push(MediaType.VIDEO)
   }
   let selections = ''
   let selectionArgs = []
   let fileKeyObj = mediaLibrary.FileKey;
   for (let i = 0;i<mediaTypes.length;i++){
     selections +=((i==0?'':' or ')+fileKeyObj.MIME_TYPE + '= ?')
     selectionArgs.push(mediaTypes[i].valueOf().toString())
   }
   let fetchOp = {
     selections:selections,
     selectionArgs:selectionArgs,
     order:fileKeyObj.DATE_ADDED+' DESC',
   };
   let fetchFileResult = await this.sysMediaLib.getFileAssets(fetchOp);
   let fileAssets = await fetchFileResult.getAllObject();
   return fileAssets;
 }

}