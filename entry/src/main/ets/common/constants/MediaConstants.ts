export interface CommonChangedListener<T>{
  (T,extra):void
}

export enum TAGS{
  CAMERA_SERVICE='[CameraService]',
  VIDEO_DECODER = '[VideoDecoder]',
  VIDEO_ENCODER = '[VideoEncoder]',
  VIDEO_RECORDER = '[VideoRecorder]',
  MEDIA_PLAYER = '[MediaPlayer]',
  MEDIA_PLAYER_LIFECYCLE = 'MediaPlayerLifeCycle',
  AUDIO_PLAYER = '[AudioPlayer]',
  AUDIO_RECORDER='[AudioRecorder]',
  AUDIO_CAPTURE = '[AudioCapture]'
}
export  enum MediaPlaySpeed{
  SPEED_FORWARD_0_75_X = 0,
  SPEED_FORWARD_1_00_X=1,
  SPEED_FORWARD_1_25_X=2,
  SPEED_FORWARD_1_75_X = 3,
  SPEED_FORWARD_2_00_X = 4
}
export enum VideoPlayGestureType{
  IDLE,
  PROGRESS_CONTROL,
  VOLUME_CONTROL,
  BRIGHT_CONTROL

}
export enum MediaPlayServiceType{
  IJK,
  HM_VIDEO_PLAYER,
  HM_AUDIO_PLAYER,
  HM_AUDIO_PLAYER_PCM,
}

export enum MediaRecodeServiceType{
  HM_VIDEO_RECORDER,
  HM_AUDIO_RECORDER,
  HM_AUDIO_RECORDER_PCM,
}

export enum MediaPlayState{
  IDLE,
  PREPARED,
  RENDER_START,
  PLAY,
  PAUSE,
  STOP,
  ERROR,
  FINISH,
  RELEASE,
  TIME_UPDATE,
  SEEK_DONE,
  DURATION_UPDATE,
  BUFFERING_START,
  BUFFERING_UPDATE,
  BUFFERING_END,
  CACHED_DURATION,
  SIZE_CHANGED
}
export enum MediaSourceType{
  DEFAULT,
  CLIENT,
  DEVICE_ABSOLUTE,
  DEVICE_ABSOLUTE_INCOMPLETE,
  RAWFILE
}
export enum AudioRenderSpeed{
  RENDER_RATE_NORMAL = 0,
  RENDER_RATE_DOUBLE=1,
  RENDER_RATE_HALF=2 ,
}

export enum MediaRecordState{
  IDLE,
  PREPARED,
  START,
  PAUSE,
  STOP,
  RELEASE,
  RESET,
  ERROR,
  TIME_UPDATE,
  COLLECT_BUFFER,
}
export enum MediaType{
  FILE,
  IMAGE,
  VIDEO,
  AUDIO,
  CAMERA,
  DOCUMENT,
  DOWNLOAD
}
export enum EncodeAction{
  INIT,
  START,
  STOP,
  TERMINATE
}

export enum DecodeAction{
  INIT,
  START,
  LOAD_BUFFER,
  STOP,
  TERMINATE
}
export enum CodeState{
  IDLE,
  PREPARED,
  STARTED,
  CODE_CHANGE,
  CODE_CHANGING,
  CODE_CHANGED,
  STOP,
  RESET,
  RELEASE,
  ERROR
}