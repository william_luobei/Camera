export class MediaOperationAsset {
  private operationId:number = -1;
  private asset:any;
  public setFd(fd:number){
    this.operationId = fd;
  }
  public getFd(){
    return this.operationId;
  }
  public setAsset(asset:any){
    this.asset = asset;
  }
  public getAsset(){
    return this.asset;
  }
}