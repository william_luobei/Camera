export class FrameConfigInfo{
  private index;
  private timeMs;
  private offset;
  private length;
  private flags;
  public setIndex(index){
    this.index=index;
  }
  public getIndex(){
    return this.index;
  }
  public setTimeMs(timeMs){
    this.timeMs = timeMs;
  }

  public getTimeMs(){
    return this.timeMs;
  }

  public setOffset(offset){
    this.offset = offset;
  }

  public getOffset(){
    return this.offset;
  }

  public setLength(length){
    this.length = length;
  }
  public getLength(){
    return this.length;
  }
  public setFlags(flags){
    this.flags = flags;
  }
  public getFlags(){
    return this.flags;
  }


}